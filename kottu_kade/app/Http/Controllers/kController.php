<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Validator;


class kController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('order');
       $orders=Order::all();
       return view('orderlist',compact('orders','orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
        {
            $request->validate([
                'f_name'=>'required',
                'l_name'=>'required',
                'adress'=>'required',
                'contact'=>'required|max:10',
                'others'=>'required',

            ]);
   
       $data =new Order([
           'f_name'=>$request->get('f_name'),
           'l_name'=>$request->get('l_name'),
           'adress'=>$request->get('adress'),
           'contact'=>$request->get('contact'),
           'others'=>$request->get('others'),
           'food'=>$request->get('food')
  
       ]);

       $data->save();
       return redirect('/kottukade')->with('sucess','Contact Saved');

   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->is('kottukade/home'))
        {
            return view('home');
        }
        if($request->is('kottukade/dspacial'))
        {
            return view('dspecial');

        }
        if($request->is('kottukade/order'))
        {
            return view('order');

        }
        if($request->is('kottukade/orderlist'))
        {
            $data=Order::all();
            return view('orderlist',['orders'=>$data]);

        }
        if($request->is('kottukade/contact'))
        {
            return view('contact');

        }
        if($request->is('/kottukade/{{ $order->id }}/edit'))
        {
            
            //return view('update');
            return Order::find($id);
            
        }

        

       

       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Order::find($id);
        return view('update',['orders'=>$data]);

        // return $data;
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'f_name'=>'required',
            'l_name'=>'required',
            'adress'=>'required',
            'contact'=>'required|max:10',
            'others'=>'required',

        ]);

   $data =Order::find($id);
   $data->f_name=$request->f_name;
   $data->l_name=$request->l_name;
   $data->adress=$request->adress;
   $data->contact=$request->contact;
   $data->others=$request->others;
   $data->food=$request->food;

   $data->save();
   return redirect('/kottukade')->with('sucess','order updated');
        
        
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Order::find($id);
        $data->delete();
     
        return redirect('/kottukade/orderlist')->with('sucess','Order delete');
    }

   
}
