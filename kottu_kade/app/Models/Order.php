<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //use HasFactory;

    protected $fillable=[
        'f_name',
        'l_name',
        'adress',
        'contact',
        'food',
        'others'
    ];
}
