@extends('layout')

@section('content')

<div class="container">
<h1>home page</h1>



<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Adress</th>
      <th scope="col">Contact Number</th>
      <th scope="col">food</th>
      <th scope="col">Notes</th>
      <th scope="col">Operation</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach($orders as $order)
  <tr>
        <td>{{ $order['id'] }}</td>
        <td>{{ $order['f_name'] }}{{ $order['l_name'] }}</td>
        <td>{{ $order['adress'] }}</td>
        <td>{{ $order['contact'] }}</td>
        <td>{{ $order['food'] }}</td>
        <td>{{ $order['others'] }}</td>
        <!-- <td><a href={{"delete/".$order['id']}}>Delete</a></td> -->
        <td>
        {!!Form::open(['url' => ['kottukade', $order->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        

        <!-- <a href={{"/kottukade/edit/".$order['id'] }} class="btn btn-success">UPDATE</a> -->
        
        
        {!!Form::close()!!}

        <!-- {!!Form::open(['url' => ['kottukade//update', $order->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{ csrf_field() }}
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('UPDATE', ['class' => 'btn btn-success'])}}


        {!!Form::close()!!} -->

        <a href="/kottukade/{{ $order['id'] }}/edit" class="btn btn-success">EDIT</a>



     </td>  
  </tr>
  @endforeach
  </tbody>
</table>


</table>
</div>
@stop