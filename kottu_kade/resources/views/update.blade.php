
@extends('layout')

@section('content')
<div class="container">

<h1>Update your order </h1>

@if($errors->any())
@foreach($errors->all() as $err)
 <div> <li><p class="text-danger">{{ $err }}</p></li></div>

@endforeach
@endif
{{ Form::open(['action' => ['App\Http\Controllers\kController@update', $orders->id],'method' => 'POST']) }}
@csrf

{{ Form::hidden('_method','PUT') }}



  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label" >First Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="First Name" name="f_name">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label" >Last Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Last Name" name="l_name">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Adress</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Adress" name="adress">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Contact Number</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" id="inputEmail3" placeholder="Contact Number" name="contact">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Note</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Note" name="others">
    </div>
  </div>



 
 
  <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Item</label>
  <select name="food" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
    <option selected>Choose...</option>
    <option value="Cheese Koththu">Cheese Koththu</option>
    <option value="Cheese Chicken Kottu">Cheese Chicken Kottu</option>
    <option value="pork Kottu">pork Kottu</option>
    <option value="veg Kottu">veg Kottu</option>
    <option value="Egg Kottu">Egg Kottu</option>
    <option value="Sea Food Kottu">Sea Food Kottu</option>
  </select>

 
  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Update Order</button>
    </div>
  </div>

</div>

{{ Form::close() }}
@stop


